import tensorflow as tf
import numpy as np
from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical

# Load and preprocess the MNIST dataset
(x_test, y_test) = mnist.load_data()[1]
x_test = x_test / 255.0  # Normalize pixel values to [0, 1]
x_test = x_test.astype(np.float32)  # Convert to FLOAT32
y_test = to_categorical(y_test)

# Reshape the data to add a channel dimension for Conv2D
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

# Load the saved model
loaded_model = tf.keras.models.load_model('tiny_cnn_model.h5')

# Perform inference using the loaded model
predictions = loaded_model.predict(x_test)

# Optionally, you can evaluate the model's accuracy on the test data
loss, accuracy = loaded_model.evaluate(x_test, y_test, verbose=0)
print(f'Original Model - Loss: {loss:.4f}, Accuracy: {accuracy * 100:.2f}%')

# Convert the model to a TensorFlow Lite model
converter = tf.lite.TFLiteConverter.from_keras_model(loaded_model)
tflite_model = converter.convert()

# Save the TensorFlow Lite model to a file
with open("tiny_cnn_model.tflite", "wb") as f:
    f.write(tflite_model)

# Load the TFLite model for inference
interpreter = tf.lite.Interpreter(model_path='tiny_cnn_model.tflite')
interpreter.allocate_tensors()

# Get input and output details
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Perform inference using the TFLite model
tflite_accuracy = 0.0
for i in range(len(x_test)):
    interpreter.set_tensor(input_details[0]['index'], x_test[i:i+1])
    interpreter.invoke()
    tflite_predictions = interpreter.get_tensor(output_details[0]['index'])
    tflite_accuracy += int(tf.argmax(tflite_predictions, axis=1).numpy()) == int(tf.argmax(y_test[i:i+1], axis=1).numpy())

tflite_accuracy /= len(x_test)
print(f'TFLite Model - Accuracy: {tflite_accuracy * 100:.2f}%')
