import tensorflow as tf
from tensorflow.keras import layers, models
from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical

# Load and preprocess the MNIST dataset
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0  # Normalize pixel values to [0, 1]

# One-hot encode the labels
y_train = to_categorical(y_train, num_classes=10)
y_test = to_categorical(y_test, num_classes=10)

# Build a modified model
input_shape = (28, 28, 1)
num_classes = 10  # The number of classes (digits)

model = models.Sequential([
    layers.Input(shape=input_shape),
    layers.Conv2D(32, (3, 3), activation='relu', padding='same'),
    layers.MaxPooling2D((2, 2), padding='same'),
    layers.Conv2D(64, (3, 3), activation='relu', padding='same'),
    layers.MaxPooling2D((2, 2), padding='same'),
    layers.Conv2D(64, (3, 3), activation='relu', padding='same'),
    layers.MaxPooling2D((2, 2), padding='same'),
    layers.Flatten(),
    layers.Dense(256, activation='relu'),
    layers.Dense(num_classes, activation='softmax')
])

model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Train the model
model.fit(x_train, y_train, epochs=5, batch_size=32, validation_split=0.2)

# Save the trained model
model.save('r_cnn_model.h5')

# Load the saved model for inference
loaded_model = tf.keras.models.load_model('r_cnn_model.h5')

# Perform inference using the loaded model
predictions = loaded_model.predict(x_test)
predicted_labels = tf.argmax(predictions, axis=1)

# Evaluate the accuracy
accuracy = tf.reduce_mean(tf.cast(predicted_labels == tf.argmax(y_test, axis=1), tf.float32))
print(f'Accuracy: {accuracy.numpy() * 100:.2f}%')
