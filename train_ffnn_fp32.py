import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Flatten, Dense
from tensorflow.keras.utils import to_categorical

# Load and preprocess the MNIST dataset
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0  # Normalize pixel values to [0, 1]
y_train, y_test = to_categorical(y_train), to_categorical(y_test)

# Build a Feed-Forward Neural Network (FFNN) model
model = Sequential([
    Flatten(input_shape=(28, 28)),  # Flatten layer removed
    Dense(128, activation='relu'),
    Dense(64, activation='relu'),
    Dense(10, activation='softmax')
])

# Compile the model
model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

# Train the model
model.fit(x_train, y_train, epochs=5, batch_size=64, validation_split=0.2)

# Save the trained model
model.save('ffnn_model.h5')

# Load the saved model
loaded_model = tf.keras.models.load_model('ffnn_model.h5')

# Perform inference on the test data using the saved model
predictions = loaded_model.predict(x_test)

# Convert predictions to class labels (0-9)
predicted_labels = tf.argmax(predictions, axis=1).numpy()

# Calculate accuracy
true_labels = tf.argmax(y_test, axis=1).numpy()
accuracy = (predicted_labels == true_labels).mean()
print(f'Saved Model Accuracy: {accuracy * 100:.2f}%')
