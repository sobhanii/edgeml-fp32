import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import load_model
from tensorflow import lite

# Load and preprocess the MNIST dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0  # Normalize pixel values to [0, 1]

# Reshape the data to add a channel dimension
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

# Load the saved model
loaded_model = load_model('squeezenet_model.h5')

# Convert the model to TFLite format
converter = lite.TFLiteConverter.from_keras_model(loaded_model)
tflite_model = converter.convert()

# Save the TFLite model to a file
with open('squeezenet_model.tflite', 'wb') as f:
    f.write(tflite_model)

# Load the TFLite model
interpreter = tf.lite.Interpreter(model_path='squeezenet_model.tflite')
interpreter.allocate_tensors()

# Get input and output tensors
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Perform inference using the TFLite model
tflite_accuracy = 0.0
for i in range(len(x_test)):
    input_data = x_test[i:i+1].astype('float32')  # Ensure data type is FLOAT32
    interpreter.set_tensor(input_details[0]['index'], input_data)
    interpreter.invoke()
    tflite_predictions = interpreter.get_tensor(output_details[0]['index'])

    # Calculate accuracy
    predicted_label = tf.argmax(tflite_predictions, axis=1).numpy()
    true_label = y_test[i]  # Use this approach for non-one-hot encoded labels
    tflite_accuracy += (predicted_label == true_label).astype(int).sum()

tflite_accuracy /= len(x_test)
print(f'TFLite Accuracy: {tflite_accuracy * 100:.2f}%')
