import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import load_model
from tensorflow.keras.utils import to_categorical

# Load and preprocess the MNIST dataset
(x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0  # Normalize pixel values to [0, 1]
y_test = to_categorical(y_test, num_classes=10)  # One-hot encode the target labels

# Reshape the data to add a channel dimension
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

# Load the saved model
loaded_model = load_model('squeezenet_model.h5')

# Perform inference using the loaded model
predictions = loaded_model.predict(x_test)

# Optionally, you can evaluate the model's accuracy on the test data
loss, accuracy = loaded_model.evaluate(x_test, y_test, verbose=0)
print(f'Loss: {loss:.4f}, Accuracy: {accuracy * 100:.2f}%')
