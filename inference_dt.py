import joblib
from sklearn.metrics import accuracy_score
from tensorflow.keras.datasets import mnist

# Load the MNIST dataset
_, (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0
x_test = x_test.reshape(x_test.shape[0], -1)

# Load the saved Decision Tree model
loaded_model = joblib.load('decision_tree_model.joblib')

# Make predictions on the test data
y_pred = loaded_model.predict(x_test)

# Calculate the accuracy of the loaded Decision Tree model
accuracy = accuracy_score(y_test, y_pred)
print(f'Accuracy (Loaded Model): {accuracy * 100:.2f}%')
