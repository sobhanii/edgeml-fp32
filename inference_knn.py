import numpy as np
from sklearn.metrics import accuracy_score
import joblib
from tensorflow.keras.datasets import mnist

# Load the MNIST dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0
x_test = x_test.reshape(x_test.shape[0], -1)

# Load the saved KNN model
loaded_knn = joblib.load('knn_model.pkl')

# Make predictions on the test data
y_pred = loaded_knn.predict(x_test)

# Calculate the accuracy of the KNN model
accuracy = accuracy_score(y_test, y_pred)
print(f'Accuracy: {accuracy * 100:.2f}%')
