import tensorflow as tf

# Load the saved model
loaded_model = tf.keras.models.load_model('neural_network_model.h5')

# Load the TFLite model
interpreter = tf.lite.Interpreter(model_path='neural_network_model.tflite')
interpreter.allocate_tensors()

input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Load and preprocess the MNIST test data
(_, _), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
x_test = x_test / 255.0
x_test = x_test.reshape(-1, 28 * 28)
x_test = x_test.astype('float32')  # Cast to FLOAT32

# Perform inference using the original model
original_predictions = loaded_model.predict(x_test)
original_accuracy = tf.keras.metrics.SparseCategoricalAccuracy()(y_test, original_predictions).numpy()
print(f"Original Model Accuracy: {original_accuracy * 100:.2f}%")

# Perform inference using the TFLite model
tflite_accuracy = 0.0
for i in range(len(x_test)):
    interpreter.set_tensor(input_details[0]['index'], x_test[i:i+1])  # Provide a single flattened image
    interpreter.invoke()
    tflite_predictions = interpreter.get_tensor(output_details[0]['index'])
    tflite_accuracy += tf.keras.metrics.SparseCategoricalAccuracy()(y_test[i:i+1], tflite_predictions).numpy()

tflite_accuracy /= len(x_test)
print(f"TFLite Model Accuracy: {tflite_accuracy * 100:.2f}%")
