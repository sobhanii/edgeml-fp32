import tensorflow as tf
from tensorflow import lite
from tensorflow.keras.datasets import mnist
from tensorflow.keras.utils import to_categorical

# Load and preprocess the MNIST test dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0
x_test = x_test.reshape(-1, 28, 28, 1)
y_test = to_categorical(y_test)

# Load the saved Keras model
loaded_model = tf.keras.models.load_model('cnn_model.h5')

# Convert the Keras model to TensorFlow Lite
converter = lite.TFLiteConverter.from_keras_model(loaded_model)
tflite_model = converter.convert()

# Save the TensorFlow Lite model to a file
with open('cnn_model.tflite', 'wb') as f:
    f.write(tflite_model)

# Load the TensorFlow Lite model
interpreter = tf.lite.Interpreter(model_path='cnn_model.tflite')
interpreter.allocate_tensors()

# Get the input and output details for the TFLite model
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Initialize accuracy counter
tflite_accuracy = 0.0

# Perform inference using the TensorFlow Lite model
for i in range(len(x_test)):
    x_test_i32 = tf.cast(x_test[i:i+1], tf.float32)  # Convert to FLOAT32
    interpreter.set_tensor(input_details[0]['index'], x_test_i32)
    interpreter.invoke()
    tflite_predictions = interpreter.get_tensor(output_details[0]['index'])
    tflite_accuracy += tf.keras.metrics.CategoricalAccuracy()(y_test[i:i+1], tflite_predictions).numpy()

tflite_accuracy /= len(x_test)
print(f'TFLite Model Accuracy: {tflite_accuracy * 100:.2f}%')
