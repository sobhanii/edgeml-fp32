import tensorflow as tf
from tensorflow.keras.datasets import mnist
from tensorflow import lite
import numpy as np

# Load and preprocess the MNIST test dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0  # Normalize pixel values to [0, 1]
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

# Load the saved model
loaded_model = tf.keras.models.load_model('simple_squeezenet_model.h5')

# Perform inference on the test data using the saved model
predictions = loaded_model.predict(x_test)

# Convert predictions to class labels (0-9)
predicted_labels = np.argmax(predictions, axis=1)

# Calculate accuracy
true_labels = np.argmax(y_test, axis=0)  # No need to specify axis here
accuracy = np.mean(predicted_labels == true_labels)
print(f'Saved Model Accuracy: {accuracy * 100:.2f}%')

# Convert the model to TFLite format
converter = lite.TFLiteConverter.from_keras_model(loaded_model)
tflite_model = converter.convert()

# Save the TFLite model to a file
with open('simple_squeezenet_model.tflite', 'wb') as f:
    f.write(tflite_model)

# Load the TFLite model
interpreter = tf.lite.Interpreter(model_path='simple_squeezenet_model.tflite')
interpreter.allocate_tensors()

# Get input and output tensors
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Perform inference using the TFLite model
tflite_accuracy = 0.0
for i in range(len(x_test)):
    input_data = x_test[i:i+1].astype('float32')
    interpreter.set_tensor(input_details[0]['index'], input_data)
    interpreter.invoke()
    tflite_predictions = interpreter.get_tensor(output_details[0]['index'])

    # Calculate accuracy
    predicted_label = np.argmax(tflite_predictions)
    true_label = np.argmax(y_test[i:i+1])
    tflite_accuracy += (predicted_label == true_label).astype(int).sum()

tflite_accuracy /= len(x_test)
print(f'TFLite Model Accuracy: {tflite_accuracy * 100:.2f}%')
