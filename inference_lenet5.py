import tensorflow as tf
from tensorflow.keras.datasets import mnist

# Load and preprocess the MNIST test dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0  # Normalize pixel values to [0, 1]
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

# Load the saved LeNet-5 model
loaded_model = tf.keras.models.load_model('lenet5_model.h5')

# Perform inference on the test data using the saved model
predictions = loaded_model.predict(x_test)

# Convert predictions to class labels (0-9)
predicted_labels = tf.argmax(predictions, axis=1).numpy()

# Calculate accuracy
accuracy = (predicted_labels == y_test).mean()
print(f'Saved Model Accuracy: {accuracy * 100:.2f}%')
