import tensorflow as tf
from tensorflow.keras.datasets import mnist
import numpy as np

# Load and preprocess the MNIST test dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0  # Normalize pixel values to [0, 1]
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)

# Load the saved model
loaded_model = tf.keras.models.load_model('simple_squeezenet_model.h5')

# Perform inference on the test data
predictions = loaded_model.predict(x_test)

# Convert predictions to class labels (0-9)
predicted_labels = np.argmax(predictions, axis=1)

# Calculate accuracy
true_labels = np.argmax(y_test, axis=0)
accuracy = np.mean(predicted_labels == true_labels)

print(f'Accuracy: {accuracy * 100:.2f}%')
