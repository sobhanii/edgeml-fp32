import tensorflow as tf
from tensorflow.keras.datasets import mnist
import numpy as np

# Load and preprocess the MNIST test dataset
(_, _), (x_test, y_test) = mnist.load_data()
x_test = x_test / 255.0  # Normalize pixel values to [0, 1]
x_test = x_test.reshape(x_test.shape[0], 28, 28, 1)
y_test = tf.keras.utils.to_categorical(y_test, 10)

# Load the saved AlexNet-like model
loaded_model = tf.keras.models.load_model('alexnet_model.h5')

# Perform inference on the test data using the saved model
predictions = loaded_model.predict(x_test)

# Convert predictions to class labels (0-9)
predicted_labels = np.argmax(predictions, axis=1)

# Calculate accuracy with the original model
accuracy = np.mean(predicted_labels == np.argmax(y_test, axis=1))
print(f'Saved Model Accuracy: {accuracy * 100:.2f}%')
